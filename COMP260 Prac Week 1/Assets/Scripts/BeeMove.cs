﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour
{
    public float speed = 4.0f;
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 direction = target.position - transform.position;
        direction = direction.normalized;

        Vector2 velocity = direction * speed;
        transform.Translate(velocity * Time.deltaTime);
    }
}
